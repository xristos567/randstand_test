<?php

namespace Drupal\event_registration\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use \Drupal\node\Entity\Node;
use Drupal\Core\Access\AccessResult;
use Drupal\Core\Session\AccountInterface;

/**
 * Provides a Event Registration form.
 */
class EventRegistrationForm extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'event_registration_event_registration';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $options = $this->getDepartments();
    $form['department'] = [
      '#title' => $this->t('Department'),
      '#type' => 'select',
      '#options' => $options,
      '#errors' => 'errorddd',
    ];

    $form['companion'] = [
      '#type' => 'textfield',
      '#title' => $this
        ->t('Companion'),
      '#size' => 60,
      '#maxlength' => 128,
    ];
    $form['children'] = array(
      '#type' => 'number',
      '#min' => 0,
      '#max' => 5,
      '#default_value' => 0,
      '#title' => $this
        ->t('Children'),
    );

    $form['actions'] = [
      '#type' => 'actions',
    ];
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Register'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $departmentId = $form_state->getValue('department');
    if (!in_array($departmentId, array_keys($this->getDepartments()))) {
      $form_state->setErrorByName('department', $this->t('Not a valid department'));
    }
    $companion = $form_state->getValue('companion');
    if(strlen(trim($companion)) > 120) {
      $form_state->setErrorByName('companion', $this->t('The companion\'s name is too long.'));
    }
    $children = $form_state->getValue('children');
    // dd(!is_numeric($children));
    if(!is_numeric($children) || intval($children) < 0 || intval($children) > 5) {
      $form_state->setErrorByName('children', $this->t('Not a valid number of children'));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $registration = Node::create([
      'type'        => 'event_registration',
      'title'       => 'Event registration',
    ]);
    $companion = trim($form_state->getValue('companion'));
    if($companion) {
      $registration->field_companion_name = $companion;
    }
    $children = intval($form_state->getValue('children'));
    $registration->field_num_children = $children;
    $registration->field_department->target_id = $form_state->getValue('department');
    $registration->save();
    $this->messenger()->addStatus($this->t('The registration submitted succesfully'));
    $form_state->setRedirect('<front>');
  }

  public function getDepartments() {
    $vid = 'department';
    $options = [];
    $terms =\Drupal::entityTypeManager()->getStorage('taxonomy_term')->loadTree($vid);
    foreach ($terms as $term) {
      $options[$term->tid] = $term->name;
    }
    return $options;
  }

  public function access(AccountInterface $account) {
    $query = \Drupal::entityQuery('node')
    ->condition('status', 1)
    ->condition('type', 'event_registration')
    ->condition('uid', $account->id());
    $nids = $query->execute();
    if(!empty($nids)) {
      $result = AccessResult::forbidden();
      $this->messenger()->addError($this->t('Already registered'));
    }
    else {
      $result = AccessResult::allowed();
    }
    return $result;
  }

}
